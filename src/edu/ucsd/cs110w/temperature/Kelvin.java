/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * TODO (jpcanuel): write class javadoc  
 * @author jpcanuel
 *
 * 
 */
public class Kelvin extends Temperature {

	public Kelvin(float t) 
	{ 
	 super(t); 
	} 

	/* (non-Javadoc)
	 * @see edu.ucsd.cs110w.temperature.Temperature#toCelsius()
	 */
	 public String toString() 
	 { 
	 // TODO: Complete this method 
		 String s1 = String.valueOf(this.value);
		 
	    return s1 + " K"; 
	 }	
	
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		Celsius k = new Celsius((this.value - 273));
		return k;
	}

	/* (non-Javadoc)
	 * @see edu.ucsd.cs110w.temperature.Temperature#toFahrenheit()
	 */
	@Override
	public Temperature toFahrenheit() {
		float c = (float)(this.value - 273.15);
		Fahrenheit f = new Fahrenheit(((c*9)/5)  + 32);
		return f;
	}
	
	public Temperature toKelvin() {
		
		return this;
	}

}
