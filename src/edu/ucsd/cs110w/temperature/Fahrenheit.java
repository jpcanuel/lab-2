package edu.ucsd.cs110w.temperature; 
public class Fahrenheit extends Temperature 
{ 
 public Fahrenheit(float t) 
 { 
 super(t); 
 } 
 public String toString() 
 { 
	 String s1 = String.valueOf(this.value);
	 
	  return s1 + " F";  
 }
@Override
public Temperature toCelsius() {
	// TODO Auto-generated method stub
	Celsius c = new Celsius( ( (this.value - 32) * 5)/9);
	return c;
}
@Override
public Temperature toFahrenheit() {
	// TODO Auto-generated method stub
	return this;
} 
public Temperature toKelvin() {
	
	return null;
}
}
